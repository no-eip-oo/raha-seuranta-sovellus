-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 16, 2023 at 10:20 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raha-sovellus`
--

-- --------------------------------------------------------

--
-- Table structure for table `kayttaja`
--

CREATE TABLE `kayttaja` (
  `ID` int(11) NOT NULL,
  `kayt_nimi` varchar(30) NOT NULL,
  `salasana` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menot`
--

CREATE TABLE `menot` (
  `tapahtuma_ID` int(11) NOT NULL,
  `kayt_id` int(11) NOT NULL,
  `menot` int(11) NOT NULL,
  `kategoria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `menot`
--

INSERT INTO `menot` (`tapahtuma_ID`, `kayt_id`, `menot`, `kategoria`) VALUES
(1, 1, 500, '');

-- --------------------------------------------------------

--
-- Table structure for table `tulot`
--

CREATE TABLE `tulot` (
  `tapahtuma_ID` int(11) NOT NULL,
  `kayt_id` int(11) NOT NULL,
  `maara` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`,`kayt_nimi`);

--
-- Indexes for table `menot`
--
ALTER TABLE `menot`
  ADD PRIMARY KEY (`tapahtuma_ID`);

--
-- Indexes for table `tulot`
--
ALTER TABLE `tulot`
  ADD PRIMARY KEY (`tapahtuma_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menot`
--
ALTER TABLE `menot`
  MODIFY `tapahtuma_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tulot`
--
ALTER TABLE `tulot`
  MODIFY `tapahtuma_ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
