window.onload = function ajastin() {
	var sekunti = 59;
	var minuutit = 3;
	document.getElementById("tarjous").innerHTML = "Erikoistarjous voimassa vain seuraavan " + String(minuutit) + " minuutin ajan!";
	function tick() {
		document.getElementById("countdown").innerHTML = String(minuutit) + ":" + (sekunti < 10 ? "0" : "") + String(sekunti);
		if(sekunti == 0 && minuutit == 0) {
			clearTimeout();
		}
		else if(sekunti == 0) {
			minuutit--;
			sekunti = 59;
			setTimeout(tick, 1000);
		}
		else {
			sekunti--;
			setTimeout(tick, 1000);
		}
	}
	tick();
}