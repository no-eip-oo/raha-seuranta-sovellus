const phpPolku = "http://localhost/raha-seuranta-sovellus/php";


function signIn() {
    const controller = `${phpPolku}/control.php`;

    console.log("Signing in...");
    $.post(controller, {
        operation: "signIn",
        kaytNimi: $("#kaytNimi").val(),
        salasana: $("#salasana").val(),
    }).done( data => {console.log(data)});
}

function addExpense() {
    const controller = `${phpPolku}/control.php`;
    console.log("Adding Expense...");
    $.post(controller, {
        operation: "addExpense",
        amount: $("#expenseInput").val(),
        category: $("#categoryInput").val(),
    }).done( data => {console.log(data)});
}

function addIncome() {
    const controller = `${phpPolku}/control.php`;
    console.log("Adding income...");
    $.post(controller, {
        operation: "addIncome",
        amount: $("#incomeInput").val(),
    }).done( data => {console.log(data)});
}

function getIncomeData() {
    const controller = `${phpPolku}/control.php`;
    console.log("Getting income data...");
    $.post(controller, {
        operation: "getIncomeData",
    }).done( data => {makeIncomeChart(data)});
}

function makeIncomeChart(jsonData) {
    jsonData = JSON.parse(jsonData);
    console.log(jsonData)

    var incomeAmountArr = [];
    for(let i = 0; i < jsonData.length; i++ ){
        incomeAmountArr.push(jsonData[i].maara);
    }

    var incomeLabelArr = [];
    for(let i = 0; i < jsonData.length; i++) {
        incomeLabelArr.push(jsonData[i].tapahtuma_ID);
    }

    const ctx = document.getElementById('tulotChart');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: incomeLabelArr,
            datasets: [{
                label: '€',
                data: incomeAmountArr,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

function getExpenseData(){
    const controller = `${phpPolku}/control.php`;
    console.log("Getting expense data...");
    $.post(controller, {
        operation: "getExpenseData",
        userID: "0",
    }).done( data => {makeExpenseChart(data)});
}

function makeExpenseChart(jsonData) {
    var jsonData = JSON.parse(jsonData);
    console.log(jsonData);

    
    var amountArr = [];
    for(let i = 0; i < jsonData.length; i++ ){
        amountArr.push(jsonData[i].menot);
    }

    var transactionArr = [];
    for(let i = 0; i < jsonData.length; i++) {
        transactionArr.push(jsonData[i].tapahtuma_ID);
    }

    const ctx = document.getElementById('menotChart');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: transactionArr,
            datasets: [{
                label: '€',
                data: amountArr,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}