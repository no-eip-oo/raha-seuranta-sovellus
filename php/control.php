<?php

/*
    Called from javascript
    Runs functions in functions.php and returns result
*/

require_once("pdo.php");
require_once("functions.php");

$operation = $_POST['operation'];
$return;

switch ($operation) {
    case 'addExpense':
        $userID = 0;
        $amount = $_POST['amount'];
        $category = $_POST['category'];
        addExpense($userID, $amount, $category);
        break;
    
    case 'addIncome':
        $userID = 0;
        $amount = $_POST['amount'];

        $return = addIncome($userID, $amount);
        break;

    case 'getIncomeData':
        $userID = 0;
        echo(json_encode(getIncomeData($userID)));
        break;

    case 'getExpenseData':
        $userID = 0;
        echo(json_encode(getExpenseData($userID)));
        break;
}

?>