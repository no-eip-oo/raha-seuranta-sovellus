<?php

/*
    Connects 
*/

function PDO() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    //$dbName = "raha-sovellus";

    try {
        $pdo = new PDO("mysql:host=$servername;dbname=raha-sovellus", $username, $password);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
    
    catch(PDOException $e) {
        echo("Connection failed: " . $e->getMessage());
    }
    
    return $pdo;

}

?>