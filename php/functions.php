<?php

function addExpense($userID, $amount, $category) {
    $pdo = PDO();
    $sql = "INSERT INTO menot (kayt_id, menot, kategoria)
            VALUES (:userID, :amount, :category)";

    $stmt = $pdo->prepare($sql);
    $stmt -> execute(array(
        ':userID' => $userID,
        ':amount' => $amount,
        ':category' => $category,
    ));
}

function addIncome($userID, $amount) {
    $pdo = PDO();
    $sql = "INSERT INTO tulot (kayt_id, maara)
            VALUES (:userID, :amount)";
    
    $stmt = $pdo->prepare($sql);
    $stmt -> execute(array(
        ':userID' => $userID,
        ':amount' => $amount,
    ));
}

function getIncomeData($userID) {
    $pdo = PDO();
    $stmt = $pdo -> query("SELECT * FROM tulot where kayt_id = $userID");
    $data = $stmt -> fetchALL(PDO::FETCH_ASSOC);
    return $data;
}

function getExpenseData($userID) {
    $pdo = PDO();
    $stmt = $pdo -> query("SELECT * FROM menot where kayt_id = $userID");
    $data = $stmt -> fetchALL(PDO::FETCH_ASSOC);
    return $data;
}
?>